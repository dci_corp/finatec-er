﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Controls" Type="Folder">
			<Item Name="Core Holder.ctl" Type="VI" URL="../Controls/Core Holder.ctl"/>
			<Item Name="Electrode Data.ctl" Type="VI" URL="../Controls/Electrode Data.ctl"/>
			<Item Name="Electrode Select.ctl" Type="VI" URL="../Controls/Electrode Select.ctl"/>
			<Item Name="ER Data.ctl" Type="VI" URL="../Controls/ER Data.ctl"/>
			<Item Name="ER Log Header UI Cluster.ctl" Type="VI" URL="../Controls/ER Log Header UI Cluster.ctl"/>
			<Item Name="Post FGV Write Action.ctl" Type="VI" URL="../Controls/Post FGV Write Action.ctl"/>
			<Item Name="Ring Control.ctl" Type="VI" URL="../Controls/Ring Control.ctl"/>
			<Item Name="Test Profile.ctl" Type="VI" URL="../Controls/Test Profile.ctl"/>
			<Item Name="Test Setup.ctl" Type="VI" URL="../Controls/Test Setup.ctl"/>
			<Item Name="Valve Boolean Control.ctl" Type="VI" URL="../Controls/Valve Boolean Control.ctl"/>
			<Item Name="Worker Cmd.ctl" Type="VI" URL="../Controls/Worker Cmd.ctl"/>
			<Item Name="Measurement Type Enum.ctl" Type="VI" URL="../Controls/Measurement Type Enum.ctl"/>
			<Item Name="browse-button.ctl" Type="VI" URL="../Controls/browse-button.ctl"/>
		</Item>
		<Item Name="Shared Modules" Type="Folder">
			<Item Name="Common Custom Controls" Type="Folder">
				<Item Name="DCF Note Cluster.ctl" Type="VI" URL="../Shared Modules/Common Custom Controls/DCF Note Cluster.ctl"/>
				<Item Name="DCF Serial Configuration Cluster.ctl" Type="VI" URL="../Shared Modules/Common Custom Controls/DCF Serial Configuration Cluster.ctl"/>
			</Item>
			<Item Name="Data Viewer" Type="Folder">
				<Item Name="SubVIs" Type="Folder"/>
				<Item Name="DCF Data Viewer.lvlib" Type="Library" URL="../Shared Modules/Data Viewer/DCF Data Viewer.lvlib"/>
			</Item>
			<Item Name="FGV" Type="Folder">
				<Item Name="channel calibration clust.ctl" Type="VI" URL="../Shared Modules/FGV/channel calibration clust.ctl"/>
				<Item Name="Channel Enum.ctl" Type="VI" URL="../Shared Modules/FGV/Channel Enum.ctl"/>
				<Item Name="Device Enum.ctl" Type="VI" URL="../Shared Modules/FGV/Device Enum.ctl"/>
				<Item Name="FGV Cmd Enum.ctl" Type="VI" URL="../Shared Modules/FGV/FGV Cmd Enum.ctl"/>
				<Item Name="FGV Main.vi" Type="VI" URL="../Shared Modules/FGV/FGV Main.vi"/>
				<Item Name="Get Chan Units.vi" Type="VI" URL="../Shared Modules/FGV/Get Chan Units.vi"/>
			</Item>
			<Item Name="Log" Type="Folder">
				<Item Name="File Log.lvlib" Type="Library" URL="../Shared Modules/Log/File Log.lvlib"/>
			</Item>
			<Item Name="Maintenance" Type="Folder">
				<Item Name="SubVIs" Type="Folder"/>
				<Item Name="Maintenance.lvlib" Type="Library" URL="../Shared Modules/Maintenance/Maintenance.lvlib"/>
			</Item>
			<Item Name="RCL" Type="Folder">
				<Item Name="Keysight RCL.lvlib" Type="Library" URL="../Shared Modules/RCL/Keysight RCL.lvlib"/>
			</Item>
			<Item Name="Worker Controls" Type="Folder">
				<Item Name="Q Cluster.ctl" Type="VI" URL="../Shared Modules/Worker Controls/Q Cluster.ctl"/>
				<Item Name="Q Ref.ctl" Type="VI" URL="../Shared Modules/Worker Controls/Q Ref.ctl"/>
			</Item>
			<Item Name="Labjack" Type="Folder">
				<Item Name="U3" Type="Folder">
					<Item Name="LabJack U3.lvlib" Type="Library" URL="../Shared Modules/Labjack/U3/LabJack U3.lvlib"/>
					<Item Name="LabJack Init Cluster.ctl" Type="VI" URL="../Shared Modules/Labjack/LabJack Init Cluster.ctl"/>
				</Item>
			</Item>
		</Item>
		<Item Name="UI Elements" Type="Folder">
			<Item Name="Basic Button Big Text.ctl" Type="VI" URL="../UI Elements/Basic Button Big Text.ctl"/>
			<Item Name="Basic Button Down.ctl" Type="VI" URL="../UI Elements/Basic Button Down.ctl"/>
			<Item Name="Basic Button None.ctl" Type="VI" URL="../UI Elements/Basic Button None.ctl"/>
			<Item Name="Basic Button Right.ctl" Type="VI" URL="../UI Elements/Basic Button Right.ctl"/>
			<Item Name="Basic Button Small.ctl" Type="VI" URL="../UI Elements/Basic Button Small.ctl"/>
			<Item Name="Baud Rate Ring.ctl" Type="VI" URL="../UI Elements/Baud Rate Ring.ctl"/>
			<Item Name="Config Button 45x44.ctl" Type="VI" URL="../../finatec-nmr/UI Elements/Config Button 45x44.ctl"/>
			<Item Name="Custom Ring.ctl" Type="VI" URL="../UI Elements/Custom Ring.ctl"/>
			<Item Name="Data Bits Ring.ctl" Type="VI" URL="../UI Elements/Data Bits Ring.ctl"/>
			<Item Name="DCF Accumulator Slide.ctl" Type="VI" URL="../UI Elements/DCF Accumulator Slide.ctl"/>
			<Item Name="External Mode Disabled.ctl" Type="VI" URL="../UI Elements/External Mode Disabled.ctl"/>
			<Item Name="External Mode On-Off.ctl" Type="VI" URL="../UI Elements/External Mode On-Off.ctl"/>
			<Item Name="File Path.ctl" Type="VI" URL="../../finatec-nmr/UI Elements/File Path.ctl"/>
			<Item Name="Flow Control Ring.ctl" Type="VI" URL="../UI Elements/Flow Control Ring.ctl"/>
			<Item Name="Gray Bar Chunking Background.ctl" Type="VI" URL="../UI Elements/Gray Bar Chunking Background.ctl"/>
			<Item Name="Log Time Medium Gray.ctl" Type="VI" URL="../UI Elements/Log Time Medium Gray.ctl"/>
			<Item Name="Log Time Medium White.ctl" Type="VI" URL="../UI Elements/Log Time Medium White.ctl"/>
			<Item Name="Log Time.ctl" Type="VI" URL="../UI Elements/Log Time.ctl"/>
			<Item Name="Logging Indicator.ctl" Type="VI" URL="../UI Elements/Logging Indicator.ctl"/>
			<Item Name="Menu Dropdown Resizable.ctl" Type="VI" URL="../UI Elements/Menu Dropdown Resizable.ctl"/>
			<Item Name="Menu Dropdown.ctl" Type="VI" URL="../../finatec-nmr/UI Elements/Menu Dropdown.ctl"/>
			<Item Name="Numeric Medium Dark.ctl" Type="VI" URL="../../finatec-nmr/UI Elements/Numeric Medium Dark.ctl"/>
			<Item Name="Numeric Medium Gray.ctl" Type="VI" URL="../UI Elements/Numeric Medium Gray.ctl"/>
			<Item Name="Numeric Medium White.ctl" Type="VI" URL="../UI Elements/Numeric Medium White.ctl"/>
			<Item Name="Numeric Medium.ctl" Type="VI" URL="../UI Elements/Numeric Medium.ctl"/>
			<Item Name="Numeric Small.ctl" Type="VI" URL="../../finatec-nmr/UI Elements/Numeric Small.ctl"/>
			<Item Name="Parity Ring.ctl" Type="VI" URL="../UI Elements/Parity Ring.ctl"/>
			<Item Name="Pressure Mode On-Off.ctl" Type="VI" URL="../../finatec-nmr/UI Elements/Pressure Mode On-Off.ctl"/>
			<Item Name="Status Boolean With Text.ctl" Type="VI" URL="../../finatec-nmr/UI Elements/Status Boolean With Text.ctl"/>
			<Item Name="Status Boolean.ctl" Type="VI" URL="../UI Elements/Status Boolean.ctl"/>
			<Item Name="Stop Bits Ring.ctl" Type="VI" URL="../UI Elements/Stop Bits Ring.ctl"/>
			<Item Name="String Resizable.ctl" Type="VI" URL="../UI Elements/String Resizable.ctl"/>
			<Item Name="String-Gray-BG.ctl" Type="VI" URL="../UI Elements/String-Gray-BG.ctl"/>
			<Item Name="String-White-BG.ctl" Type="VI" URL="../UI Elements/String-White-BG.ctl"/>
			<Item Name="Switch Button.ctl" Type="VI" URL="../UI Elements/Switch Button.ctl"/>
			<Item Name="VPA Active State 31x31.ctl" Type="VI" URL="../../finatec-nmr/UI Elements/VPA Active State 31x31.ctl"/>
			<Item Name="radio-buttons.ctl" Type="VI" URL="../Finatec/Controls/radio-buttons.ctl"/>
		</Item>
		<Item Name="Finatec" Type="Folder">
			<Item Name="Finatec.lvclass" Type="LVClass" URL="../Finatec/Finatec.lvclass"/>
		</Item>
		<Item Name="SubVIs" Type="Folder">
			<Item Name="Find Range.vi" Type="VI" URL="../SubVIs/Find Range.vi"/>
			<Item Name="Format Number.vi" Type="VI" URL="../SubVIs/Format Number.vi"/>
			<Item Name="Input Frequency Filenames.vi" Type="VI" URL="../SubVIs/Input Frequency Filenames.vi"/>
		</Item>
		<Item Name="Base System Class.lvclass" Type="LVClass" URL="../Base System Class/Base System Class.lvclass"/>
		<Item Name="DCI.ico" Type="Document" URL="../DCI.ico"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="MGI Exit if Runtime.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Exit if Runtime.vi"/>
				<Item Name="MGI VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference.vi"/>
				<Item Name="MGI Caller&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Caller&apos;s VI Reference.vi"/>
				<Item Name="MGI Current VI&apos;s Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Current VI&apos;s Reference.vi"/>
				<Item Name="MGI Top Level VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Top Level VI Reference.vi"/>
				<Item Name="MGI Level&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Level&apos;s VI Reference.vi"/>
				<Item Name="MGI Insert Reserved Error.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Insert Reserved Error.vi"/>
				<Item Name="MGI Create Directory Chain Behavior Enum.ctl" Type="VI" URL="/&lt;userlib&gt;/_MGI/File/MGI Create Directory Chain Behavior Enum.ctl"/>
				<Item Name="MGI Create Directory Chain.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/File/MGI Create Directory Chain.vi"/>
				<Item Name="MGI RWA Convertion Direction Enum.ctl" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Convertion Direction Enum.ctl"/>
				<Item Name="MGI RWA Options Cluster.ctl" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Options Cluster.ctl"/>
				<Item Name="MGI RWA Tag Lookup Cluster.ctl" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Tag Lookup Cluster.ctl"/>
				<Item Name="MGI RWA Handle Tag or Refnum.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Handle Tag or Refnum.vi"/>
				<Item Name="MGI RWA Unreplace Characters.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Unreplace Characters.vi"/>
				<Item Name="MGI Get Cluster Elements.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Cluster/MGI Get Cluster Elements.vi"/>
				<Item Name="MGI U8 Data to Hex Str.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI U8 Data to Hex Str.vi"/>
				<Item Name="MGI RWA Build Line.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Build Line.vi"/>
				<Item Name="MGI RWA Replace Characters.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Replace Characters.vi"/>
				<Item Name="MGI RWA Get Type Info.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Get Type Info.vi"/>
				<Item Name="MGI RWA Process Array Elements.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Process Array Elements.vi"/>
				<Item Name="MGI Windows Regional Ring.ctl" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Windows Get Regional String/MGI Windows Regional Ring.ctl"/>
				<Item Name="MGI Windows Get Regional String.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Windows Get Regional String.vi"/>
				<Item Name="MGI RWA Remove EOLs and Slashes.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Remove EOLs and Slashes.vi"/>
				<Item Name="MGI RWA Enque Top Level Data.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Enque Top Level Data.vi"/>
				<Item Name="MGI RWA Anything to String.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Anything to String.vi"/>
				<Item Name="MGI Suppress Error Code (Scalar).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Suppress Error Code/MGI Suppress Error Code (Scalar).vi"/>
				<Item Name="MGI Suppress Error Code (Array).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Suppress Error Code/MGI Suppress Error Code (Array).vi"/>
				<Item Name="MGI Suppress Error Code.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Suppress Error Code.vi"/>
				<Item Name="MGI RWA Write Strings to File.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Write Strings to File.vi"/>
				<Item Name="MGI Write Anything.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI Write Anything.vi"/>
				<Item Name="Valid Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Valid Path - Traditional__ogtk.vi"/>
				<Item Name="Valid Path - Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Valid Path - Array__ogtk.vi"/>
				<Item Name="Valid Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Valid Path__ogtk.vi"/>
				<Item Name="File Exists - Scalar__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/File Exists - Scalar__ogtk.vi"/>
				<Item Name="File Exists - Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/File Exists - Array__ogtk.vi"/>
				<Item Name="File Exists__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/File Exists__ogtk.vi"/>
				<Item Name="MGI Get Executable Version.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Get Executable Version.vi"/>
				<Item Name="MGI Read Anything.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI Read Anything.vi"/>
				<Item Name="MGI RWA Read Strings from File.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Read Strings from File.vi"/>
				<Item Name="MGI RWA String To Anything.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA String To Anything.vi"/>
				<Item Name="MGI RWA INI Tag Lookup.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA INI Tag Lookup.vi"/>
				<Item Name="MGI Scan From String.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String.vi"/>
				<Item Name="MGI Scan From String (CDB).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (CDB).vi"/>
				<Item Name="MGI Scan From String (CDB[]).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (CDB[]).vi"/>
				<Item Name="MGI Scan From String (CSG).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (CSG).vi"/>
				<Item Name="MGI Scan From String (CSG[]).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (CSG[]).vi"/>
				<Item Name="MGI Scan From String (CXT).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (CXT).vi"/>
				<Item Name="MGI Scan From String (CXT[]).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (CXT[]).vi"/>
				<Item Name="MGI Scan From String (DBL[]).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Scan From String/MGI Scan From String (DBL[]).vi"/>
				<Item Name="MGI RWA Unprocess Array Elements.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Unprocess Array Elements.vi"/>
				<Item Name="MGI RWA Build Array Name.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Read Write Anything/MGI RWA Build Array Name.vi"/>
				<Item Name="MGI Hex Str to U8 Data.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/String/MGI Hex Str to U8 Data.vi"/>
				<Item Name="MGI Change Detector.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Change Detector.vi"/>
				<Item Name="MGI Change Detector (T).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Change Detector/MGI Change Detector (T).vi"/>
				<Item Name="MGI Change Detector (F).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Change Detector/MGI Change Detector (F).vi"/>
				<Item Name="MGI Wait.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Timing/MGI Wait.vi"/>
				<Item Name="MGI Wait (U32).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Timing/MGI Wait/MGI Wait (U32).vi"/>
				<Item Name="MGI Wait (Double).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Timing/MGI Wait/MGI Wait (Double).vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="DTbl Empty Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Empty Digital.vi"/>
				<Item Name="DWDT Empty Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Empty Digital.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Stall Data Flow.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Stall Data Flow.vim"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Read Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet.vi"/>
				<Item Name="Read Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Read Lines From File (with error IO).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File (with error IO).vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Read Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (I64).vi"/>
				<Item Name="Read Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (string).vi"/>
				<Item Name="Write Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet.vi"/>
				<Item Name="Write Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (I64).vi"/>
				<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
				<Item Name="Create File and Containing Folders.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Create File and Containing Folders.vi"/>
				<Item Name="Create Directory Recursive.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Create Directory Recursive.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="labjackud.dll" Type="Document" URL="labjackud.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Finatec ER" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{299B910C-321E-45D6-B6B9-7C1174AE5EEE}</Property>
				<Property Name="App_INI_GUID" Type="Str">{DD304B01-7203-41F3-A51E-30A4EADCFAB1}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">1</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{649B05F8-6411-4FCF-BCFB-45508D5F60B3}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Finatec ER</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/E/LV Builds/Finatec</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{369E08E2-AC09-48C3-802C-A1906173A63E}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Bld_version.patch" Type="Int">2</Property>
				<Property Name="Destination[0].destName" Type="Str">Finatec ER.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/E/LV Builds/Finatec/NI_AB_PROJECTNAME.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/E/LV Builds/Finatec/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/DCI.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{05D6545B-4F08-45A5-B1BB-ABDB385A6060}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Finatec/Finatec.lvclass/Finatec Main.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Shared Modules/Labjack/U3/LabJack U3.lvlib/LabJackUD.dll</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">DCI</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Finatec ER</Property>
				<Property Name="TgtF_internalName" Type="Str">Finatec ER</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017 DCI</Property>
				<Property Name="TgtF_productName" Type="Str">Finatec ER</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{74A30851-89EB-4677-9703-FCC9B4ADEBC0}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Finatec ER.exe</Property>
			</Item>
			<Item Name="Finatec ER Installer with MAX" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">Finatec ER</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{250FC371-B890-4858-BAAB-C1E60596F6BB}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[0].productID" Type="Str">{CB6C2533-4926-42B8-AC21-04BB9679F818}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI-488.2 Runtime 20.0</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{357F6618-C660-41A2-A185-5578CC876D1D}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[1].productID" Type="Str">{F12C6F92-5B1C-4EAB-9364-96026CE1920D}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI-Serial Runtime 20.0</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{01D82F43-B48D-46FF-8601-FC4FAAE20F41}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[2].productID" Type="Str">{A36800D3-08A2-44C9-B12D-F6753A895660}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI-VISA Runtime 21.0</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[3].productID" Type="Str">{6B440D80-3B0D-43B2-8A06-E2E939AA1006}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI LabVIEW Runtime 2020 SP1</Property>
				<Property Name="DistPart[3].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[3].SoftDep[0].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[3].SoftDep[0].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[3].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[3].SoftDep[1].productName" Type="Str">NI Deployment Framework 2020</Property>
				<Property Name="DistPart[3].SoftDep[1].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[3].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[3].SoftDep[10].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[3].SoftDep[10].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[3].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[3].SoftDep[11].productName" Type="Str">NI TDM Streaming 19.0</Property>
				<Property Name="DistPart[3].SoftDep[11].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[3].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[3].SoftDep[2].productName" Type="Str">NI Error Reporting 2020</Property>
				<Property Name="DistPart[3].SoftDep[2].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[3].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[3].SoftDep[3].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2020</Property>
				<Property Name="DistPart[3].SoftDep[3].upgradeCode" Type="Str">{00D0B680-F876-4E42-A25F-52B65418C2A6}</Property>
				<Property Name="DistPart[3].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[3].SoftDep[4].productName" Type="Str">NI LabVIEW Runtime 2020 SP1 Non-English Support.</Property>
				<Property Name="DistPart[3].SoftDep[4].upgradeCode" Type="Str">{61FCC73D-8092-457D-8905-27C0060D88E1}</Property>
				<Property Name="DistPart[3].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[3].SoftDep[5].productName" Type="Str">NI Logos 20.0</Property>
				<Property Name="DistPart[3].SoftDep[5].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[3].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[3].SoftDep[6].productName" Type="Str">NI LabVIEW Web Server 2020</Property>
				<Property Name="DistPart[3].SoftDep[6].upgradeCode" Type="Str">{0960380B-EA86-4E0C-8B57-14CD8CCF2C15}</Property>
				<Property Name="DistPart[3].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[3].SoftDep[7].productName" Type="Str">NI mDNS Responder 19.0</Property>
				<Property Name="DistPart[3].SoftDep[7].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[3].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[3].SoftDep[8].productName" Type="Str">Math Kernel Libraries 2017</Property>
				<Property Name="DistPart[3].SoftDep[8].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[3].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[3].SoftDep[9].productName" Type="Str">Math Kernel Libraries 2020</Property>
				<Property Name="DistPart[3].SoftDep[9].upgradeCode" Type="Str">{9872BBBA-FB96-42A4-80A2-9605AC5CBCF1}</Property>
				<Property Name="DistPart[3].SoftDepCount" Type="Int">12</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{D84FC73F-D1E0-4C05-A30C-DB882CD1ABD8}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[4].productID" Type="Str">{68AA4B28-8BE7-4281-A587-2079311AC1CB}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI Measurement &amp; Automation Explorer 21.0</Property>
				<Property Name="DistPart[4].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[0].productName" Type="Str">NI Error Reporting</Property>
				<Property Name="DistPart[4].SoftDep[0].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[4].SoftDepCount" Type="Int">1</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{AE940F24-CC0E-4148-9A96-10FB04D9796D}</Property>
				<Property Name="DistPartCount" Type="Int">5</Property>
				<Property Name="INST_author" Type="Str">DCI</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">/E/LV Builds/Finatec ER/1.0.2.0 Full Installer Finatec ER</Property>
				<Property Name="INST_buildSpecName" Type="Str">Finatec ER Installer with MAX</Property>
				<Property Name="INST_defaultDir" Type="Str">{250FC371-B890-4858-BAAB-C1E60596F6BB}</Property>
				<Property Name="INST_installerName" Type="Str">setup.exe</Property>
				<Property Name="INST_productName" Type="Str">Finatec ER</Property>
				<Property Name="INST_productVersion" Type="Str">1.1.18</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">20008011</Property>
				<Property Name="MSI_arpCompany" Type="Str">DCI</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.dcitestsystems.com/</Property>
				<Property Name="MSI_distID" Type="Str">{A825F842-6673-49EE-9D32-B89154756428}</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{82EFD206-D893-42DD-A9D2-D1F0103DFF84}</Property>
				<Property Name="MSI_windowMessage" Type="Str">This wizard will install/update Ambient/Overburden ER software</Property>
				<Property Name="MSI_windowTitle" Type="Str">Ambient/Overburden ER</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{250FC371-B890-4858-BAAB-C1E60596F6BB}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{250FC371-B890-4858-BAAB-C1E60596F6BB}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">Finatec ER.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">Overburden ER</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">ONGC Overburden ER</Property>
				<Property Name="Source[0].File[0].Shortcut[1].destIndex" Type="Int">1</Property>
				<Property Name="Source[0].File[0].Shortcut[1].name" Type="Str">Overburden ER Desktop</Property>
				<Property Name="Source[0].File[0].Shortcut[1].subDir" Type="Str"></Property>
				<Property Name="Source[0].File[0].Shortcut[2].destIndex" Type="Int">2</Property>
				<Property Name="Source[0].File[0].Shortcut[2].name" Type="Str">Overburden ER Startup</Property>
				<Property Name="Source[0].File[0].Shortcut[2].subDir" Type="Str"></Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">3</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{74A30851-89EB-4677-9703-FCC9B4ADEBC0}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">Finatec ER</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/Finatec ER</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
			<Item Name="Finatec ER Mini-Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">Finatec ER</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{250FC371-B890-4858-BAAB-C1E60596F6BB}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="INST_author" Type="Str">DCI</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">/E/LV Builds/Finatec ER/Mini-Installer</Property>
				<Property Name="INST_buildSpecName" Type="Str">Finatec ER Mini-Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{250FC371-B890-4858-BAAB-C1E60596F6BB}</Property>
				<Property Name="INST_installerName" Type="Str">setup.exe</Property>
				<Property Name="INST_productName" Type="Str">Finatec ER</Property>
				<Property Name="INST_productVersion" Type="Str">1.1.45</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">19008009</Property>
				<Property Name="MSI_arpCompany" Type="Str">DCI</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.dcitestsystems.com/</Property>
				<Property Name="MSI_distID" Type="Str">{983B378C-49EB-4CCC-9A2D-76C57210B670}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{3CE1444B-041F-414C-B79C-EB8C89F16725}</Property>
				<Property Name="MSI_windowTitle" Type="Str">Ambient/Overburden ER</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{250FC371-B890-4858-BAAB-C1E60596F6BB}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{250FC371-B890-4858-BAAB-C1E60596F6BB}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">Finatec ER.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">Ambiet-Overburden ER</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">ONGC Overburden ER</Property>
				<Property Name="Source[0].File[0].Shortcut[1].destIndex" Type="Int">1</Property>
				<Property Name="Source[0].File[0].Shortcut[1].name" Type="Str">Ambient-Overburden ER Desktop</Property>
				<Property Name="Source[0].File[0].Shortcut[1].subDir" Type="Str"></Property>
				<Property Name="Source[0].File[0].Shortcut[2].destIndex" Type="Int">2</Property>
				<Property Name="Source[0].File[0].Shortcut[2].name" Type="Str">Ambient-Overburden ER Startup</Property>
				<Property Name="Source[0].File[0].Shortcut[2].subDir" Type="Str"></Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">3</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{74A30851-89EB-4677-9703-FCC9B4ADEBC0}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">Finatec ER</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/Finatec ER</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
